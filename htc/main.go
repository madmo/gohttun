package main

import (
	"code.google.com/p/go.net/websocket"
	"io"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
)

func HttpTunnel(ProxyAddr, TargetAddr string) (io.ReadWriteCloser, error) {
	p, err := net.Dial("tcp", ProxyAddr)
	if err != nil {
		return nil, err
	}

	turl, err := url.Parse(TargetAddr)
	if err != nil {
		return nil, err
	}

	req := http.Request{
		Method: "CONNECT",
		URL:    &url.URL{},
		Host:   turl.Host,
	}

	cc := httputil.NewProxyClientConn(p, nil)
	cc.Do(&req)
	if err != nil && err != httputil.ErrPersistEOF {
		return nil, err
	}

	rwc, _ := cc.Hijack()

	return rwc, nil
}

func ConnectTunnel(RemoteAddr string) (io.ReadWriteCloser, error) {
	if os.Getenv("HTTP_PROXY") == "" {
		return websocket.Dial(RemoteAddr, "", RemoteAddr)
	}

	purl, err := url.Parse(os.Getenv("HTTP_PROXY"))
	if err != nil {
		return nil, err
	}

	config, err := websocket.NewConfig(RemoteAddr, RemoteAddr)
	if err != nil {
		return nil, err
	}

	client, err := HttpTunnel(purl.Host, RemoteAddr)
	if err != nil {
		return nil, err
	}

	return websocket.NewClient(config, client)
}

func main() {
	if (len(os.Args) != 3) {
		os.Stderr.Write([]byte("Usage: " + os.Args[0] + " remote listenaddr\n"))
		os.Exit(2)
	}

	listener, err := net.Listen("tcp", os.Args[2])
	if err != nil {
		panic(err)
	}
	defer listener.Close()

	for {
		os.Stdout.Write([]byte("Waiting for connection...\n"))
		conn, err := listener.Accept()
		if err != nil {
			panic(err)
		}
		ws, err := ConnectTunnel("ws://" + os.Args[1])
		if err != nil {
			panic(err)
		}

		go io.Copy(conn, ws)
		io.Copy(ws, conn)

		ws.Close()
		conn.Close()
	}
}
