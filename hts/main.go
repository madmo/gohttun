package main

import (
	"code.google.com/p/go.net/websocket"
	"flag"
	"io"
	"log"
	"net"
	"net/http"
	"syscall"
)

var (
	httpAddr = flag.String("http", ":443", "http listen address")
	destAddr = flag.String("dest", "127.0.0.1:22", "dest connect address")
)

func dropRoot() {
	if syscall.Getuid() == 0 {
		syscall.Setgid(65534)
		syscall.Setuid(65534)
	}
	if syscall.Getuid() == 0 {
		panic("still root")
	}
}

func TunnelServer(ws *websocket.Conn) {
	c, err := net.Dial("tcp", *destAddr)
	if err != nil {
		log.Println("Can't get target conn:", err)
		return
	}
	defer c.Close()

	go io.Copy(ws, c)
	io.Copy(c, ws)
}

func main() {
	flag.Parse()

	http.Handle("/", websocket.Handler(TunnelServer))

	l, err := net.Listen("tcp", *httpAddr)
	if err != nil {
		panic(err)
	}

	dropRoot()

	http.Serve(l, nil)
}
